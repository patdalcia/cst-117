﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CST_117_Topic_6_Programming_Project_4
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            
        }

        private Label[,] labels = null;

        private void NewGameButton_Click(object sender, EventArgs e)
        {
            bool flag = false;
            Random rand = new Random();
            int[,] board = new int[3, 3];
            int i = 0;
            int j = 0;

            for (i = 0; i < board.GetLength(0); i++)
            {
                for (j = 0; j < board.GetLength(0); j++)
                {
                    board[i, j] = rand.Next(0, 2);

                    if (board[i, j] == 1)
                    {
                        labels[i, j].Text = "X";
                    }
                    else
                    {
                        labels[i, j].Text = "O";
                    }
                }
            }
            int xCount = 0;
            int oCount = 0;

            /*Testing for a horizontal win*/
            for (i = 0; i < board.GetLength(0); i++)
            {
                for (j = 0; j < board.GetLength(0); j++)
                {
                    if(labels[i, j].Text == "X") { xCount++; }
                    else { oCount++; }
                }
                if(oCount == 3) { resultLabel.Text = "O wins"; flag = true; break; }
                else if(xCount == 3) { resultLabel.Text = "X wins"; flag = true; break; }
                else { oCount = 0; xCount = 0; }
            }

            /*testing for a vertical win*/
            if (flag != true)
            {
                for (i = 0; i < board.GetLength(0); i++)
                {
                    for (j = 0; j < board.GetLength(0); j++)
                    {
                        if (labels[j, i].Text == "X") { xCount++; }
                        else { oCount++; }
                    }
                    if (oCount == 3) { resultLabel.Text = "O wins"; flag = true; break; }
                    else if (xCount == 3) { resultLabel.Text = "X wins"; flag = true; break; }
                    else { oCount = 0; xCount = 0; }
                }
            }

            /*Testing diagonal win conditions*/
            if (flag != true)
            {
                if (topLeft.Text == "X" && centerCenter.Text == "X" && bottomRight.Text == "X") { resultLabel.Text = "X wins"; }
                else if (topRight.Text == "X" && centerCenter.Text == "X" && bottomLeft.Text == "X") { resultLabel.Text = "X wins"; }
                else if (topRight.Text == "O" && centerCenter.Text == "O" && bottomLeft.Text == "O") { resultLabel.Text = "O wins"; }
                else if (topLeft.Text == "O" && centerCenter.Text == "O" && bottomRight.Text == "O") { resultLabel.Text = "O wins"; }

                else { resultLabel.Text = "The game has resulted in a tie!"; }
            }



        }
        
        private void Form1_Load(object sender, EventArgs e)
        {
            labels = new Label[3,3];
            labels[0, 0] = topLeft;
            labels[0, 1] = topCenter;
            labels[0, 2] = topRight;
            labels[1, 0] = centerLeft;
            labels[1, 1] = centerCenter;
            labels[1, 2] = centerRight;
            labels[2, 0] = bottomLeft;
            labels[2, 1] = bottomCenter;
            labels[2, 2] = bottomRight;
        }

        private void ExitButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
