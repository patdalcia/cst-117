﻿namespace CST_117_Topic_6_Programming_Project_4
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.topLeft = new System.Windows.Forms.Label();
            this.newGameButton = new System.Windows.Forms.Button();
            this.exitButton = new System.Windows.Forms.Button();
            this.resultLabel = new System.Windows.Forms.Label();
            this.topCenter = new System.Windows.Forms.Label();
            this.topRight = new System.Windows.Forms.Label();
            this.centerLeft = new System.Windows.Forms.Label();
            this.centerCenter = new System.Windows.Forms.Label();
            this.centerRight = new System.Windows.Forms.Label();
            this.bottomLeft = new System.Windows.Forms.Label();
            this.bottomCenter = new System.Windows.Forms.Label();
            this.bottomRight = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // topLeft
            // 
            this.topLeft.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.topLeft.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.topLeft.Location = new System.Drawing.Point(47, 20);
            this.topLeft.Name = "topLeft";
            this.topLeft.Size = new System.Drawing.Size(37, 32);
            this.topLeft.TabIndex = 0;
            this.topLeft.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // newGameButton
            // 
            this.newGameButton.Location = new System.Drawing.Point(25, 216);
            this.newGameButton.Name = "newGameButton";
            this.newGameButton.Size = new System.Drawing.Size(75, 23);
            this.newGameButton.TabIndex = 9;
            this.newGameButton.Text = "New Game";
            this.newGameButton.UseVisualStyleBackColor = true;
            this.newGameButton.Click += new System.EventHandler(this.NewGameButton_Click);
            // 
            // exitButton
            // 
            this.exitButton.Location = new System.Drawing.Point(173, 216);
            this.exitButton.Name = "exitButton";
            this.exitButton.Size = new System.Drawing.Size(75, 23);
            this.exitButton.TabIndex = 10;
            this.exitButton.Text = "Exit";
            this.exitButton.UseVisualStyleBackColor = true;
            this.exitButton.Click += new System.EventHandler(this.ExitButton_Click);
            // 
            // resultLabel
            // 
            this.resultLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.resultLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.resultLabel.Location = new System.Drawing.Point(12, 160);
            this.resultLabel.Name = "resultLabel";
            this.resultLabel.Size = new System.Drawing.Size(253, 23);
            this.resultLabel.TabIndex = 11;
            this.resultLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // topCenter
            // 
            this.topCenter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.topCenter.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.topCenter.Location = new System.Drawing.Point(109, 20);
            this.topCenter.Name = "topCenter";
            this.topCenter.Size = new System.Drawing.Size(37, 32);
            this.topCenter.TabIndex = 12;
            this.topCenter.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // topRight
            // 
            this.topRight.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.topRight.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.topRight.Location = new System.Drawing.Point(173, 20);
            this.topRight.Name = "topRight";
            this.topRight.Size = new System.Drawing.Size(37, 32);
            this.topRight.TabIndex = 13;
            this.topRight.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // centerLeft
            // 
            this.centerLeft.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.centerLeft.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.centerLeft.Location = new System.Drawing.Point(47, 66);
            this.centerLeft.Name = "centerLeft";
            this.centerLeft.Size = new System.Drawing.Size(37, 32);
            this.centerLeft.TabIndex = 14;
            this.centerLeft.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // centerCenter
            // 
            this.centerCenter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.centerCenter.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.centerCenter.Location = new System.Drawing.Point(109, 66);
            this.centerCenter.Name = "centerCenter";
            this.centerCenter.Size = new System.Drawing.Size(37, 32);
            this.centerCenter.TabIndex = 15;
            this.centerCenter.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // centerRight
            // 
            this.centerRight.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.centerRight.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.centerRight.Location = new System.Drawing.Point(173, 66);
            this.centerRight.Name = "centerRight";
            this.centerRight.Size = new System.Drawing.Size(37, 32);
            this.centerRight.TabIndex = 16;
            this.centerRight.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // bottomLeft
            // 
            this.bottomLeft.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.bottomLeft.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bottomLeft.Location = new System.Drawing.Point(47, 111);
            this.bottomLeft.Name = "bottomLeft";
            this.bottomLeft.Size = new System.Drawing.Size(37, 32);
            this.bottomLeft.TabIndex = 17;
            this.bottomLeft.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // bottomCenter
            // 
            this.bottomCenter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.bottomCenter.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bottomCenter.Location = new System.Drawing.Point(109, 111);
            this.bottomCenter.Name = "bottomCenter";
            this.bottomCenter.Size = new System.Drawing.Size(37, 32);
            this.bottomCenter.TabIndex = 18;
            this.bottomCenter.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // bottomRight
            // 
            this.bottomRight.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.bottomRight.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bottomRight.Location = new System.Drawing.Point(173, 111);
            this.bottomRight.Name = "bottomRight";
            this.bottomRight.Size = new System.Drawing.Size(37, 32);
            this.bottomRight.TabIndex = 19;
            this.bottomRight.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(277, 272);
            this.Controls.Add(this.bottomRight);
            this.Controls.Add(this.bottomCenter);
            this.Controls.Add(this.bottomLeft);
            this.Controls.Add(this.centerRight);
            this.Controls.Add(this.centerCenter);
            this.Controls.Add(this.centerLeft);
            this.Controls.Add(this.topRight);
            this.Controls.Add(this.topCenter);
            this.Controls.Add(this.resultLabel);
            this.Controls.Add(this.exitButton);
            this.Controls.Add(this.newGameButton);
            this.Controls.Add(this.topLeft);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label topLeft;
        private System.Windows.Forms.Button newGameButton;
        private System.Windows.Forms.Button exitButton;
        private System.Windows.Forms.Label resultLabel;
        private System.Windows.Forms.Label topCenter;
        private System.Windows.Forms.Label topRight;
        private System.Windows.Forms.Label centerLeft;
        private System.Windows.Forms.Label centerCenter;
        private System.Windows.Forms.Label centerRight;
        private System.Windows.Forms.Label bottomLeft;
        private System.Windows.Forms.Label bottomCenter;
        private System.Windows.Forms.Label bottomRight;
    }
}

