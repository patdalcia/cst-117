﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CST_117_Topic_3_Exercise_5
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void CalculateButton_Click(object sender, EventArgs e)
        {
            try
            {
                double num = 4.0; //Numerator
                double den = 1.0; //Denominator
                double pi = 0.0;
                double i = 0.0;

                int terms = Int32.Parse(termsBox.Text);
                for(int x = 1; x <= terms; x++)
                {
                    i = num / den;
                    if(x % 2 == 0)
                    {
                        pi = pi - i;
                    }
                    else
                    {
                        pi = pi + i;
                    }
                    den = den + 2.0;
                }
                resultsLabel.Text = "Approximate Value of Pi after " + terms.ToString() + " terms = \n" + pi.ToString();
            }
            catch(FormatException)
            {
                MessageBox.Show("Only whole numbers can be inputted!!");
            }
        }
    }
}
