﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CST_117_Topic_4_Milestone_2_
{
    class item
    {

        static void Main(string[] args)
        {
            //Creating test item objects
            item item_1 = new item("TEST NAME", 1111, "TEST DEPARTMENT");
            item item_2 = new item("test name", 2222, "test department");

            //Showing contents of class objects on the console
            Console.WriteLine(item_1.toString());
            Console.WriteLine(item_2.toString());

            String s = "";
            while(s != "q")
            {
                Console.WriteLine("Enter 'q' to quit."); //This keeps the console open so results can be seen 
                s = Console.ReadLine();
            }
        }

        private String itemName { get; set; }
        private int quantity { get; set; }
        private String department { get; set; }

        public item(String name, int q, String d) //Constructor
        {
            this.itemName = name;
            this.quantity = q;
            this.department = d;
        }
        public item() { } //Default constructor

        /*custom toString so I can customize the output*/
        public String toString()
        {
            return "Item Name: " + this.itemName + " Quantity: " + this.quantity + " Department: " + this.department;
        }


        
    }
}
