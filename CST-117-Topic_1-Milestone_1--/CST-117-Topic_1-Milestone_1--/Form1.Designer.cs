﻿namespace CST_117_Topic_1_Milestone_1__
{
    partial class viewExistingInventory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.viewInventoryBox = new System.Windows.Forms.ComboBox();
            this.editButton = new System.Windows.Forms.Button();
            this.searchBox = new System.Windows.Forms.TextBox();
            this.inventoryDataView = new System.Windows.Forms.DataGridView();
            this.productName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.price = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.numUnits = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.department = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.description = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            ((System.ComponentModel.ISupportInitialize)(this.inventoryDataView)).BeginInit();
            this.SuspendLayout();
            // 
            // viewInventoryBox
            // 
            this.viewInventoryBox.FormattingEnabled = true;
            this.viewInventoryBox.Location = new System.Drawing.Point(52, 21);
            this.viewInventoryBox.Name = "viewInventoryBox";
            this.viewInventoryBox.Size = new System.Drawing.Size(121, 21);
            this.viewInventoryBox.TabIndex = 0;
            this.viewInventoryBox.Text = "Sort By..";
            // 
            // editButton
            // 
            this.editButton.AutoEllipsis = true;
            this.editButton.Location = new System.Drawing.Point(320, 22);
            this.editButton.Name = "editButton";
            this.editButton.Size = new System.Drawing.Size(121, 21);
            this.editButton.TabIndex = 1;
            this.editButton.Text = "Edit";
            this.editButton.UseVisualStyleBackColor = true;
            // 
            // searchBox
            // 
            this.searchBox.Location = new System.Drawing.Point(614, 22);
            this.searchBox.Name = "searchBox";
            this.searchBox.Size = new System.Drawing.Size(121, 20);
            this.searchBox.TabIndex = 2;
            this.searchBox.Text = "Search";
            // 
            // inventoryDataView
            // 
            this.inventoryDataView.AllowUserToDeleteRows = false;
            this.inventoryDataView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.inventoryDataView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.inventoryDataView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.productName,
            this.price,
            this.numUnits,
            this.department,
            this.description});
            this.inventoryDataView.Location = new System.Drawing.Point(52, 66);
            this.inventoryDataView.Name = "inventoryDataView";
            this.inventoryDataView.ReadOnly = true;
            this.inventoryDataView.Size = new System.Drawing.Size(683, 372);
            this.inventoryDataView.TabIndex = 3;
            this.inventoryDataView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView1_CellContentClick);
            // 
            // productName
            // 
            this.productName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.productName.HeaderText = "ProductName";
            this.productName.Name = "productName";
            this.productName.ReadOnly = true;
            this.productName.Width = 97;
            // 
            // price
            // 
            this.price.HeaderText = "Price";
            this.price.Name = "price";
            this.price.ReadOnly = true;
            // 
            // numUnits
            // 
            this.numUnits.HeaderText = "# Units";
            this.numUnits.Name = "numUnits";
            this.numUnits.ReadOnly = true;
            // 
            // department
            // 
            this.department.HeaderText = "Department";
            this.department.Name = "department";
            this.department.ReadOnly = true;
            // 
            // description
            // 
            this.description.HeaderText = "Description";
            this.description.Name = "description";
            this.description.ReadOnly = true;
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Location = new System.Drawing.Point(642, 50);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(93, 13);
            this.linkLabel1.TabIndex = 4;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Advanced Search";
            // 
            // viewExistingInventory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.linkLabel1);
            this.Controls.Add(this.inventoryDataView);
            this.Controls.Add(this.searchBox);
            this.Controls.Add(this.editButton);
            this.Controls.Add(this.viewInventoryBox);
            this.Name = "viewExistingInventory";
            this.Text = "View Existing Inventory";
            ((System.ComponentModel.ISupportInitialize)(this.inventoryDataView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox viewInventoryBox;
        private System.Windows.Forms.Button editButton;
        private System.Windows.Forms.TextBox searchBox;
        private System.Windows.Forms.DataGridView inventoryDataView;
        private System.Windows.Forms.DataGridViewTextBoxColumn productName;
        private System.Windows.Forms.DataGridViewTextBoxColumn price;
        private System.Windows.Forms.DataGridViewTextBoxColumn numUnits;
        private System.Windows.Forms.DataGridViewTextBoxColumn department;
        private System.Windows.Forms.DataGridViewTextBoxColumn description;
        private System.Windows.Forms.LinkLabel linkLabel1;
    }
}

