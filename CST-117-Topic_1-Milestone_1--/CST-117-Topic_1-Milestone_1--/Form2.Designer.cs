﻿namespace CST_117_Topic_1_Milestone_1__
{
    partial class editInventory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.inventoryDataView = new System.Windows.Forms.DataGridView();
            this.editButton = new System.Windows.Forms.Button();
            this.viewInventoryBox = new System.Windows.Forms.ComboBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.productName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.price = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.numUnits = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.department = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.description = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.adminOptions = new System.Windows.Forms.DataGridViewButtonColumn();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.inventoryDataView)).BeginInit();
            this.SuspendLayout();
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Location = new System.Drawing.Point(649, 46);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(93, 13);
            this.linkLabel1.TabIndex = 9;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Advanced Search";
            // 
            // inventoryDataView
            // 
            this.inventoryDataView.AllowUserToDeleteRows = false;
            this.inventoryDataView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.inventoryDataView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.inventoryDataView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.productName,
            this.price,
            this.numUnits,
            this.department,
            this.description,
            this.adminOptions});
            this.inventoryDataView.Location = new System.Drawing.Point(59, 62);
            this.inventoryDataView.Name = "inventoryDataView";
            this.inventoryDataView.ReadOnly = true;
            this.inventoryDataView.Size = new System.Drawing.Size(645, 372);
            this.inventoryDataView.TabIndex = 8;
            this.inventoryDataView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.InventoryDataView_CellContentClick);
            // 
            // editButton
            // 
            this.editButton.AutoEllipsis = true;
            this.editButton.Location = new System.Drawing.Point(327, 18);
            this.editButton.Name = "editButton";
            this.editButton.Size = new System.Drawing.Size(121, 21);
            this.editButton.TabIndex = 6;
            this.editButton.Text = "Add Item";
            this.editButton.UseVisualStyleBackColor = true;
            // 
            // viewInventoryBox
            // 
            this.viewInventoryBox.FormattingEnabled = true;
            this.viewInventoryBox.Location = new System.Drawing.Point(59, 17);
            this.viewInventoryBox.Name = "viewInventoryBox";
            this.viewInventoryBox.Size = new System.Drawing.Size(121, 21);
            this.viewInventoryBox.TabIndex = 5;
            this.viewInventoryBox.Text = "Sort By..";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(621, 23);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(121, 20);
            this.textBox1.TabIndex = 10;
            // 
            // productName
            // 
            this.productName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.productName.HeaderText = "ProductName";
            this.productName.Name = "productName";
            this.productName.ReadOnly = true;
            this.productName.Width = 97;
            // 
            // price
            // 
            this.price.HeaderText = "Price";
            this.price.Name = "price";
            this.price.ReadOnly = true;
            // 
            // numUnits
            // 
            this.numUnits.HeaderText = "# Units";
            this.numUnits.Name = "numUnits";
            this.numUnits.ReadOnly = true;
            // 
            // department
            // 
            this.department.HeaderText = "Department";
            this.department.Name = "department";
            this.department.ReadOnly = true;
            // 
            // description
            // 
            this.description.HeaderText = "Description";
            this.description.Name = "description";
            this.description.ReadOnly = true;
            // 
            // adminOptions
            // 
            this.adminOptions.HeaderText = "Options";
            this.adminOptions.Name = "adminOptions";
            this.adminOptions.ReadOnly = true;
            this.adminOptions.Text = "Delete";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(601, 83);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(103, 21);
            this.button1.TabIndex = 11;
            this.button1.Text = "DELETE";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // editInventory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.linkLabel1);
            this.Controls.Add(this.inventoryDataView);
            this.Controls.Add(this.editButton);
            this.Controls.Add(this.viewInventoryBox);
            this.Name = "editInventory";
            this.Text = "Edit Inventory";
            ((System.ComponentModel.ISupportInitialize)(this.inventoryDataView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.DataGridView inventoryDataView;
        private System.Windows.Forms.Button editButton;
        private System.Windows.Forms.ComboBox viewInventoryBox;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.DataGridViewTextBoxColumn productName;
        private System.Windows.Forms.DataGridViewTextBoxColumn price;
        private System.Windows.Forms.DataGridViewTextBoxColumn numUnits;
        private System.Windows.Forms.DataGridViewTextBoxColumn department;
        private System.Windows.Forms.DataGridViewTextBoxColumn description;
        private System.Windows.Forms.DataGridViewButtonColumn adminOptions;
        private System.Windows.Forms.Button button1;
    }
}