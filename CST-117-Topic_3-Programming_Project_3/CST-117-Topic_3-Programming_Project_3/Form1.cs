﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CST_117_Topic_3_Programming_Project_3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
          
            DialogResult r = openFileDialog1.ShowDialog();

            if(r == DialogResult.OK) //When OK is clicked
            {
                String path = openFileDialog1.FileName; // Geting filepath

                try {
                    String text = File.ReadAllText(path); //Reading file
                    text = text.ToLower(); //Converting to lowercase
                    String[] words = text.Split();
                    var sortedWords = from w in words orderby w select w; //Sorting words alphabetically

                    /*Displaying results */
                    lowercaseLabel.Text = text;
                    firstWordLabel.Text = sortedWords.First();
                    lastWordLabel.Text = sortedWords.Last();

                    sortedWords = words.OrderBy(n => n.Length); //Organizing by word length
                    var longest = sortedWords.LastOrDefault();
                    longestWordLabel.Text = longest; //Displaying longest word

                    var regex = new Regex("(a|e|i|o|u)"); //Establishing vowels
                    var mostVowels = words.Max(y => regex.Matches(y).Count); //Finding highest vowel count
                    var result = words.First(x => regex.Matches(x).Count == mostVowels); //Find word with highest count

                    vowelLabel.Text = result;//Display results 


                    using (StreamWriter writetext = new StreamWriter("example.txt"))
                    {
                        /*Writing to file called example.txt*/
                        writetext.WriteLine("Text Converted to Lowercase: " + text);
                        writetext.WriteLine("First Word Alphabetically: " + sortedWords.First());
                        writetext.WriteLine("Last Word Alphabetically: " + sortedWords.Last());
                        writetext.WriteLine("Longest word: " + longest);
                        writetext.WriteLine("Word with the most Vowels: " + result);
                    }
                }
                catch (FileNotFoundException)
                {
                    Console.WriteLine("The file or directory cannot be found.");
                }
                   

            }
        }

        private void Label1_Click(object sender, EventArgs e)
        {

        }
    }
}
