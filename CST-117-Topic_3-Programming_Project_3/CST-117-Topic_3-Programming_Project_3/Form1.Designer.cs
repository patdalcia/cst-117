﻿namespace CST_117_Topic_3_Programming_Project_3
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.selectFileButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lowercaseLabel = new System.Windows.Forms.Label();
            this.fwLabel = new System.Windows.Forms.Label();
            this.lwLabel = new System.Windows.Forms.Label();
            this.longWordLabel = new System.Windows.Forms.Label();
            this.firstWordLabel = new System.Windows.Forms.Label();
            this.lastWordLabel = new System.Windows.Forms.Label();
            this.longestWordLabel = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.vowelLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // selectFileButton
            // 
            this.selectFileButton.Location = new System.Drawing.Point(72, 305);
            this.selectFileButton.Name = "selectFileButton";
            this.selectFileButton.Size = new System.Drawing.Size(75, 23);
            this.selectFileButton.TabIndex = 0;
            this.selectFileButton.Text = "Select File";
            this.selectFileButton.UseVisualStyleBackColor = true;
            this.selectFileButton.Click += new System.EventHandler(this.Button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(150, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Text Converted to Lowercase:";
            // 
            // lowercaseLabel
            // 
            this.lowercaseLabel.AutoSize = true;
            this.lowercaseLabel.Location = new System.Drawing.Point(169, 45);
            this.lowercaseLabel.Margin = new System.Windows.Forms.Padding(3, 0, 50, 0);
            this.lowercaseLabel.Name = "lowercaseLabel";
            this.lowercaseLabel.Size = new System.Drawing.Size(0, 13);
            this.lowercaseLabel.TabIndex = 2;
            // 
            // fwLabel
            // 
            this.fwLabel.AutoSize = true;
            this.fwLabel.Location = new System.Drawing.Point(12, 95);
            this.fwLabel.Name = "fwLabel";
            this.fwLabel.Size = new System.Drawing.Size(123, 13);
            this.fwLabel.TabIndex = 3;
            this.fwLabel.Text = "First word Alphabetically:";
            // 
            // lwLabel
            // 
            this.lwLabel.AutoSize = true;
            this.lwLabel.Location = new System.Drawing.Point(12, 154);
            this.lwLabel.Name = "lwLabel";
            this.lwLabel.Size = new System.Drawing.Size(127, 13);
            this.lwLabel.TabIndex = 4;
            this.lwLabel.Text = "Last Word Alphabetically:";
            // 
            // longWordLabel
            // 
            this.longWordLabel.AutoSize = true;
            this.longWordLabel.Location = new System.Drawing.Point(12, 209);
            this.longWordLabel.Name = "longWordLabel";
            this.longWordLabel.Size = new System.Drawing.Size(77, 13);
            this.longWordLabel.TabIndex = 5;
            this.longWordLabel.Text = "Longest Word:";
            this.longWordLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // firstWordLabel
            // 
            this.firstWordLabel.AutoSize = true;
            this.firstWordLabel.Location = new System.Drawing.Point(169, 95);
            this.firstWordLabel.Name = "firstWordLabel";
            this.firstWordLabel.Size = new System.Drawing.Size(0, 13);
            this.firstWordLabel.TabIndex = 6;
            // 
            // lastWordLabel
            // 
            this.lastWordLabel.AutoSize = true;
            this.lastWordLabel.Location = new System.Drawing.Point(169, 154);
            this.lastWordLabel.Name = "lastWordLabel";
            this.lastWordLabel.Size = new System.Drawing.Size(0, 13);
            this.lastWordLabel.TabIndex = 7;
            // 
            // longestWordLabel
            // 
            this.longestWordLabel.AutoSize = true;
            this.longestWordLabel.Location = new System.Drawing.Point(169, 209);
            this.longestWordLabel.Name = "longestWordLabel";
            this.longestWordLabel.Size = new System.Drawing.Size(0, 13);
            this.longestWordLabel.TabIndex = 9;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 251);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(141, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Word with the most Vowels: ";
            // 
            // vowelLabel
            // 
            this.vowelLabel.AutoSize = true;
            this.vowelLabel.Location = new System.Drawing.Point(172, 251);
            this.vowelLabel.Name = "vowelLabel";
            this.vowelLabel.Size = new System.Drawing.Size(0, 13);
            this.vowelLabel.TabIndex = 11;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(845, 450);
            this.Controls.Add(this.vowelLabel);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.longestWordLabel);
            this.Controls.Add(this.lastWordLabel);
            this.Controls.Add(this.firstWordLabel);
            this.Controls.Add(this.longWordLabel);
            this.Controls.Add(this.lwLabel);
            this.Controls.Add(this.fwLabel);
            this.Controls.Add(this.lowercaseLabel);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.selectFileButton);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button selectFileButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lowercaseLabel;
        private System.Windows.Forms.Label fwLabel;
        private System.Windows.Forms.Label lwLabel;
        private System.Windows.Forms.Label longWordLabel;
        private System.Windows.Forms.Label firstWordLabel;
        private System.Windows.Forms.Label lastWordLabel;
        private System.Windows.Forms.Label longestWordLabel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label vowelLabel;
    }
}

