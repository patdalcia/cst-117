﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CST_117_Topic_6_Milestone_3
{
    class InventoryManager
    {

        private item[] inventory { get; set; }

        public InventoryManager() { inventory = new item[50]; } //Default Constructor
        public InventoryManager(item[] i)
        {
            this.inventory = i;
        }

        public void addItem()
        {
            /*Getting item information to be added to inventory*/
            Console.WriteLine("!!!!!!Adding a new item to the inventory!!!!!!\n");
            String name, quantity, department; 
            Console.WriteLine("Enter Item Name: ");
            name = Console.ReadLine();
            Console.WriteLine("Enter Quantity of item: ");
            quantity = Console.ReadLine();
            Console.WriteLine("Enter Item department: ");
            department = Console.ReadLine();
            
            /*Iterating through array until empty spot is found*/
            for(int i = 0; i < inventory.Length; i++)
            {
                if(inventory[i] == null)
                {
                    inventory[i] = new item(name, Int32.Parse(quantity),department); //Adding item
                    Console.WriteLine("Item has been added to inventory!\n");
                    return;
                }
            }
            /*if code reaches this point then no open space has been found in inventory,
             * inventory size will need to be adjusted to hold new items.. Might consider using list
             * for dynamic size*/
            Console.WriteLine("Item has not been added to inventory. Inventory might be full!\n");
        }

        public void removeItem()
        {
            Console.WriteLine("!!!!!!Removing item from inventory!!!!!!\n");
            /*Getting index of item to be removed*/
            int index = this.search();

            if(index == -1) { return; } //indicates that item was not found
            else
            {
                inventory[index] = null;
                Console.WriteLine("Item has been deleted from inventory\n");
                return;
            }
        }

        public void restock()
        {
            Console.WriteLine("!!!!!!Restocking an item in the inventory!!!!!!\n");
            /*Getting index of item to be removed*/
            int index = this.search();
            

            if (index == -1) { return; } //indicates that item was not found
            else
            {
                Console.WriteLine("Enter the new quantity of this item: ");
                String s = Console.ReadLine();
                if (s.All(char.IsDigit))
                {
                    int q = Int32.Parse(s);
                    inventory[index].setQuantity(q);
                    Console.WriteLine("Item has been restocked!\n");
                    return;
                }
                else
                {
                    Console.WriteLine("Only whole postive numbers may be inputted, please try again\n");
                }
            }
        }

        public void displayItems()
        {
            Console.WriteLine("!!!!!!Displaying all inventory items!!!!!!\n");
            for(int i = 0; i < inventory.Length; i++)
            {
                if(inventory[i] != null)
                {
                    Console.WriteLine(inventory[i].toString());
                }
            }
        }

        public int search()
        {
            String search;
            Console.WriteLine("Items can be searched for by the exact Item name, or the items department: ");
            search = Console.ReadLine();

            /*Iterating through array until matching name is found*/
            try
            {
                for (int i = 0; i < inventory.Length; i++)
                {

                    if (inventory[i].getName().ToLower() == search.ToLower()) //searching by name
                    {
                        Console.WriteLine("ITEM FOUND: " + inventory[i].toString());
                        return i; //returning index of found item
                    }
                    else if (inventory[i].getDepartment().ToLower() == search.ToLower()) //searching by department
                    {
                        Console.WriteLine("ITEM FOUND: " + inventory[i].toString());
                        return i;//returning index of found item
                    }
                }
            }
            catch (System.NullReferenceException)
            {
                Console.WriteLine("Incorrect search criteria! Please try again.\n");
            }

            Console.WriteLine("ITEM NOT FOUND: Please try again with different search criteria.\n");
            return -1;
        }
    }
}
