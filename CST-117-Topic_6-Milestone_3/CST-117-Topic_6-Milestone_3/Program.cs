﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CST_117_Topic_6_Milestone_3
{
    class Program
    {
        static void Main(string[] args)
        {
            InventoryManager manager = new InventoryManager();

            /*Adding item to inventory*/
            manager.addItem();
            /*Displaying all items in inventory*/
            manager.displayItems();

            /*Restocking an item within inventory. This method utilizes the search method to find the desired item. Then that item can be restocked*/
            manager.restock();

            /*Displaying results*/
            manager.displayItems();

            /*Removing item from inventory. For this method to work, it calls the search() method within 
             the inventory manager class. The Search() method returns the found index where the desired item resides.
             The removeItem() method then removes the item at said index.*/
            manager.removeItem();

            /*Displaying final results*/
            manager.displayItems();
        }
    }
}
