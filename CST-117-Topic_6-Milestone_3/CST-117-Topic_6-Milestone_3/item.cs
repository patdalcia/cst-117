﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CST_117_Topic_6_Milestone_3
{
    class item
    {
        private String itemName { get; set; }
        private int quantity { get; set; }
        private String department { get; set; }

        public item(String name, int q, String d) //Constructor
        {
            this.itemName = name;
            this.quantity = q;
            this.department = d;
        }
        public item() { } //Default constructor

        public String getName()
        {
            return this.itemName;
        }

        public void setName(String name)
        {
            this.itemName = name;
        }

        public String getDepartment()
        {
            return this.department;
        }

        public void setDepartment(String d)
        {
            this.department = d;
        }
        
        public int getQuantity()
        {
            return this.quantity;
        }

        public void setQuantity(int q)
        {
            this.quantity = q;
        }
        /*custom toString so I can customize the output*/
        public String toString()
        {
            return "Item Name: " + this.itemName + " Quantity: " + this.quantity + " Department: " + this.department + "\n";
        }
    }
}
