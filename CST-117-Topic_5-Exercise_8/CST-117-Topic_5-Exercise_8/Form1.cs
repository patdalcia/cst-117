﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CST_117_Topic_5_Exercise_8
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void CalculateButton_Click(object sender, EventArgs e)
        {
            double fatGrams = Convert.ToDouble(fatTextBox.Text);
            double carbGrams = Convert.ToDouble(carbTextBox.Text);
            fatCalOutputLabel.Text = FatCalories(fatGrams).ToString();
            carbCalOutputLabel.Text = CarbCalories(carbGrams).ToString();
        }


        public double FatCalories(double fatGrams)
        {
            return fatGrams * 9;
        }

        public double CarbCalories(double carbGrams)
        {
            return carbGrams * 4;
        }
    }
}
