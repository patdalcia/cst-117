﻿namespace CST_117_Topic_5_Exercise_8
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.calculateButton = new System.Windows.Forms.Button();
            this.fatTextBox = new System.Windows.Forms.TextBox();
            this.carbTextBox = new System.Windows.Forms.TextBox();
            this.leftLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.fatCalOutputLabel = new System.Windows.Forms.Label();
            this.carbCalOutputLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // calculateButton
            // 
            this.calculateButton.Location = new System.Drawing.Point(12, 150);
            this.calculateButton.Name = "calculateButton";
            this.calculateButton.Size = new System.Drawing.Size(75, 23);
            this.calculateButton.TabIndex = 0;
            this.calculateButton.Text = "Calculate";
            this.calculateButton.UseVisualStyleBackColor = true;
            this.calculateButton.Click += new System.EventHandler(this.CalculateButton_Click);
            // 
            // fatTextBox
            // 
            this.fatTextBox.Location = new System.Drawing.Point(12, 81);
            this.fatTextBox.Name = "fatTextBox";
            this.fatTextBox.Size = new System.Drawing.Size(100, 20);
            this.fatTextBox.TabIndex = 1;
            // 
            // carbTextBox
            // 
            this.carbTextBox.Location = new System.Drawing.Point(149, 81);
            this.carbTextBox.Name = "carbTextBox";
            this.carbTextBox.Size = new System.Drawing.Size(100, 20);
            this.carbTextBox.TabIndex = 2;
            // 
            // leftLabel
            // 
            this.leftLabel.AutoSize = true;
            this.leftLabel.Location = new System.Drawing.Point(12, 62);
            this.leftLabel.Name = "leftLabel";
            this.leftLabel.Size = new System.Drawing.Size(94, 13);
            this.leftLabel.TabIndex = 3;
            this.leftLabel.Text = "# of fat grams/day";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(146, 62);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "# of carb grams/day";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(341, 84);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Calories from fat:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(341, 140);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(99, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Calories from carbs:";
            // 
            // fatCalOutputLabel
            // 
            this.fatCalOutputLabel.AutoSize = true;
            this.fatCalOutputLabel.Location = new System.Drawing.Point(481, 83);
            this.fatCalOutputLabel.Name = "fatCalOutputLabel";
            this.fatCalOutputLabel.Size = new System.Drawing.Size(0, 13);
            this.fatCalOutputLabel.TabIndex = 7;
            // 
            // carbCalOutputLabel
            // 
            this.carbCalOutputLabel.AutoSize = true;
            this.carbCalOutputLabel.Location = new System.Drawing.Point(484, 139);
            this.carbCalOutputLabel.Name = "carbCalOutputLabel";
            this.carbCalOutputLabel.Size = new System.Drawing.Size(0, 13);
            this.carbCalOutputLabel.TabIndex = 8;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(662, 206);
            this.Controls.Add(this.carbCalOutputLabel);
            this.Controls.Add(this.fatCalOutputLabel);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.leftLabel);
            this.Controls.Add(this.carbTextBox);
            this.Controls.Add(this.fatTextBox);
            this.Controls.Add(this.calculateButton);
            this.Name = "Form1";
            this.Text = "Calorie Converter";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button calculateButton;
        private System.Windows.Forms.TextBox fatTextBox;
        private System.Windows.Forms.TextBox carbTextBox;
        private System.Windows.Forms.Label leftLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label fatCalOutputLabel;
        private System.Windows.Forms.Label carbCalOutputLabel;
    }
}

