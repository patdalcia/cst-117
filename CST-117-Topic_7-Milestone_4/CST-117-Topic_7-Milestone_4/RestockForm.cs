﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CST_117_Topic_7_Milestone_4
{
    public partial class RestockForm : Form
    {
        private bool flag;

        public RestockForm()
        {
            InitializeComponent();
        }

        private void RestockConfirmButton_Click(object sender, EventArgs e)
        {
            
            if (!String.IsNullOrWhiteSpace(restockItemTextBox.Text))
            {
                if (Int32.TryParse(restockItemTextBox.Text, out int i) && i >= 0)
                {
                    flag = true;
                    Close();
                }
                else
                {
                    MessageBox.Show("Only numbers whole postive numbers may be inputted.");
                }
            }
            else
            {
                MessageBox.Show("Fields were left blank! Please try again.");
            }
        }

        public bool getFlag()
        {
            return this.flag;
        }

        public String getData()
        {
            return restockItemTextBox.Text;
        }

        private void RestockCancelButton_Click(object sender, EventArgs e)
        {
            restockItemTextBox.Clear();
            Close();
        }
    }
}
