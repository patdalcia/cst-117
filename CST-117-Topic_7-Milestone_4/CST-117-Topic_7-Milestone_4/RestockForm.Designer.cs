﻿namespace CST_117_Topic_7_Milestone_4
{
    partial class RestockForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.label6 = new System.Windows.Forms.Label();
            this.restockItemTextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.restockCancelButton = new System.Windows.Forms.Button();
            this.restockConfirmButton = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Location = new System.Drawing.Point(58, 42);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.AutoScroll = true;
            this.splitContainer1.Panel1.BackColor = System.Drawing.Color.IndianRed;
            this.splitContainer1.Panel1.Controls.Add(this.label6);
            this.splitContainer1.Panel1.Controls.Add(this.restockItemTextBox);
            this.splitContainer1.Panel1.Controls.Add(this.label5);
            this.splitContainer1.Panel1.Controls.Add(this.label1);
            this.splitContainer1.Panel1.ForeColor = System.Drawing.SystemColors.Control;
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.BackColor = System.Drawing.Color.White;
            this.splitContainer1.Panel2.Controls.Add(this.restockCancelButton);
            this.splitContainer1.Panel2.Controls.Add(this.restockConfirmButton);
            this.splitContainer1.Panel2.Controls.Add(this.label4);
            this.splitContainer1.Panel2.Controls.Add(this.label3);
            this.splitContainer1.Panel2.Controls.Add(this.label2);
            this.splitContainer1.Size = new System.Drawing.Size(684, 366);
            this.splitContainer1.SplitterDistance = 418;
            this.splitContainer1.TabIndex = 4;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Franklin Gothic Medium", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(90, 236);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(226, 15);
            this.label6.TabIndex = 6;
            this.label6.Text = "Only whole positive numbers may be entered.";
            // 
            // restockItemTextBox
            // 
            this.restockItemTextBox.BackColor = System.Drawing.SystemColors.Control;
            this.restockItemTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.restockItemTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.restockItemTextBox.Location = new System.Drawing.Point(159, 194);
            this.restockItemTextBox.Name = "restockItemTextBox";
            this.restockItemTextBox.Size = new System.Drawing.Size(220, 17);
            this.restockItemTextBox.TabIndex = 2;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(17, 193);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(136, 18);
            this.label5.TabIndex = 1;
            this.label5.Text = "Enter new Quantity:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Franklin Gothic Medium", 22F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(13, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(346, 38);
            this.label1.TabIndex = 0;
            this.label1.Text = "Restock item in Inventory";
            // 
            // restockCancelButton
            // 
            this.restockCancelButton.BackColor = System.Drawing.Color.IndianRed;
            this.restockCancelButton.FlatAppearance.BorderColor = System.Drawing.Color.IndianRed;
            this.restockCancelButton.FlatAppearance.BorderSize = 0;
            this.restockCancelButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightCoral;
            this.restockCancelButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightCoral;
            this.restockCancelButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.restockCancelButton.Font = new System.Drawing.Font("Franklin Gothic Medium", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.restockCancelButton.ForeColor = System.Drawing.Color.White;
            this.restockCancelButton.Location = new System.Drawing.Point(16, 236);
            this.restockCancelButton.Name = "restockCancelButton";
            this.restockCancelButton.Size = new System.Drawing.Size(225, 40);
            this.restockCancelButton.TabIndex = 5;
            this.restockCancelButton.Text = "Cancel";
            this.restockCancelButton.UseVisualStyleBackColor = false;
            this.restockCancelButton.Click += new System.EventHandler(this.RestockCancelButton_Click);
            // 
            // restockConfirmButton
            // 
            this.restockConfirmButton.BackColor = System.Drawing.Color.IndianRed;
            this.restockConfirmButton.FlatAppearance.BorderColor = System.Drawing.Color.IndianRed;
            this.restockConfirmButton.FlatAppearance.BorderSize = 0;
            this.restockConfirmButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightCoral;
            this.restockConfirmButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightCoral;
            this.restockConfirmButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.restockConfirmButton.Font = new System.Drawing.Font("Franklin Gothic Medium", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.restockConfirmButton.ForeColor = System.Drawing.Color.White;
            this.restockConfirmButton.Location = new System.Drawing.Point(16, 155);
            this.restockConfirmButton.Name = "restockConfirmButton";
            this.restockConfirmButton.Size = new System.Drawing.Size(225, 40);
            this.restockConfirmButton.TabIndex = 4;
            this.restockConfirmButton.Text = "Confirm";
            this.restockConfirmButton.UseVisualStyleBackColor = false;
            this.restockConfirmButton.Click += new System.EventHandler(this.RestockConfirmButton_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Franklin Gothic Medium", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(81, 73);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(97, 15);
            this.label4.TabIndex = 2;
            this.label4.Text = "or cancel changes.";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Franklin Gothic Medium", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(19, 58);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(222, 15);
            this.label3.TabIndex = 1;
            this.label3.Text = "Click the appropriate button to either confirm";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Franklin Gothic Medium", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.IndianRed;
            this.label2.Location = new System.Drawing.Point(17, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(229, 25);
            this.label2.TabIndex = 0;
            this.label2.Text = "Inventory Manager Portal";
            // 
            // RestockForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.splitContainer1);
            this.Name = "RestockForm";
            this.Text = "RestockForm";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox restockItemTextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button restockConfirmButton;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button restockCancelButton;
    }
}