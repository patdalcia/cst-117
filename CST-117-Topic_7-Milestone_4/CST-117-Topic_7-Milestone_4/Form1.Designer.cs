﻿namespace CST_117_Topic_7_Milestone_4
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.label1 = new System.Windows.Forms.Label();
            this.searchItemButton = new System.Windows.Forms.Button();
            this.displayItemsButton = new System.Windows.Forms.Button();
            this.restockItemButton = new System.Windows.Forms.Button();
            this.removeItemButton = new System.Windows.Forms.Button();
            this.addItemButton = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Location = new System.Drawing.Point(61, 45);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.AutoScroll = true;
            this.splitContainer1.Panel1.BackColor = System.Drawing.Color.IndianRed;
            this.splitContainer1.Panel1.Controls.Add(this.label1);
            this.splitContainer1.Panel1.ForeColor = System.Drawing.SystemColors.Control;
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.BackColor = System.Drawing.Color.White;
            this.splitContainer1.Panel2.Controls.Add(this.searchItemButton);
            this.splitContainer1.Panel2.Controls.Add(this.displayItemsButton);
            this.splitContainer1.Panel2.Controls.Add(this.restockItemButton);
            this.splitContainer1.Panel2.Controls.Add(this.removeItemButton);
            this.splitContainer1.Panel2.Controls.Add(this.addItemButton);
            this.splitContainer1.Panel2.Controls.Add(this.label4);
            this.splitContainer1.Panel2.Controls.Add(this.label3);
            this.splitContainer1.Panel2.Controls.Add(this.label2);
            this.splitContainer1.Size = new System.Drawing.Size(684, 366);
            this.splitContainer1.SplitterDistance = 418;
            this.splitContainer1.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Franklin Gothic Medium", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(36, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(193, 47);
            this.label1.TabIndex = 0;
            this.label1.Text = "Welcome!";
            // 
            // searchItemButton
            // 
            this.searchItemButton.BackColor = System.Drawing.Color.IndianRed;
            this.searchItemButton.FlatAppearance.BorderColor = System.Drawing.Color.IndianRed;
            this.searchItemButton.FlatAppearance.BorderSize = 0;
            this.searchItemButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightCoral;
            this.searchItemButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightCoral;
            this.searchItemButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.searchItemButton.Font = new System.Drawing.Font("Franklin Gothic Medium", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchItemButton.ForeColor = System.Drawing.Color.White;
            this.searchItemButton.Location = new System.Drawing.Point(19, 303);
            this.searchItemButton.Name = "searchItemButton";
            this.searchItemButton.Size = new System.Drawing.Size(225, 40);
            this.searchItemButton.TabIndex = 7;
            this.searchItemButton.Text = "Search for an item";
            this.searchItemButton.UseVisualStyleBackColor = false;
            this.searchItemButton.Click += new System.EventHandler(this.SearchItemButton_Click);
            // 
            // displayItemsButton
            // 
            this.displayItemsButton.BackColor = System.Drawing.Color.IndianRed;
            this.displayItemsButton.FlatAppearance.BorderColor = System.Drawing.Color.IndianRed;
            this.displayItemsButton.FlatAppearance.BorderSize = 0;
            this.displayItemsButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightCoral;
            this.displayItemsButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightCoral;
            this.displayItemsButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.displayItemsButton.Font = new System.Drawing.Font("Franklin Gothic Medium", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.displayItemsButton.ForeColor = System.Drawing.Color.White;
            this.displayItemsButton.Location = new System.Drawing.Point(19, 255);
            this.displayItemsButton.Name = "displayItemsButton";
            this.displayItemsButton.Size = new System.Drawing.Size(225, 40);
            this.displayItemsButton.TabIndex = 6;
            this.displayItemsButton.Text = "Display all items";
            this.displayItemsButton.UseVisualStyleBackColor = false;
            this.displayItemsButton.Click += new System.EventHandler(this.DisplayItemsButton_Click);
            // 
            // restockItemButton
            // 
            this.restockItemButton.BackColor = System.Drawing.Color.IndianRed;
            this.restockItemButton.FlatAppearance.BorderColor = System.Drawing.Color.IndianRed;
            this.restockItemButton.FlatAppearance.BorderSize = 0;
            this.restockItemButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightCoral;
            this.restockItemButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightCoral;
            this.restockItemButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.restockItemButton.Font = new System.Drawing.Font("Franklin Gothic Medium", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.restockItemButton.ForeColor = System.Drawing.Color.White;
            this.restockItemButton.Location = new System.Drawing.Point(19, 209);
            this.restockItemButton.Name = "restockItemButton";
            this.restockItemButton.Size = new System.Drawing.Size(225, 40);
            this.restockItemButton.TabIndex = 5;
            this.restockItemButton.Text = "Restock an item";
            this.restockItemButton.UseVisualStyleBackColor = false;
            this.restockItemButton.Click += new System.EventHandler(this.RestockItemButton_Click);
            // 
            // removeItemButton
            // 
            this.removeItemButton.BackColor = System.Drawing.Color.IndianRed;
            this.removeItemButton.FlatAppearance.BorderColor = System.Drawing.Color.IndianRed;
            this.removeItemButton.FlatAppearance.BorderSize = 0;
            this.removeItemButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightCoral;
            this.removeItemButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightCoral;
            this.removeItemButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.removeItemButton.Font = new System.Drawing.Font("Franklin Gothic Medium", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.removeItemButton.ForeColor = System.Drawing.Color.White;
            this.removeItemButton.Location = new System.Drawing.Point(19, 163);
            this.removeItemButton.Name = "removeItemButton";
            this.removeItemButton.Size = new System.Drawing.Size(225, 40);
            this.removeItemButton.TabIndex = 4;
            this.removeItemButton.Text = "Remove an Item";
            this.removeItemButton.UseVisualStyleBackColor = false;
            this.removeItemButton.Click += new System.EventHandler(this.RemoveItemButton_Click);
            // 
            // addItemButton
            // 
            this.addItemButton.BackColor = System.Drawing.Color.IndianRed;
            this.addItemButton.FlatAppearance.BorderColor = System.Drawing.Color.IndianRed;
            this.addItemButton.FlatAppearance.BorderSize = 0;
            this.addItemButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightCoral;
            this.addItemButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightCoral;
            this.addItemButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.addItemButton.Font = new System.Drawing.Font("Franklin Gothic Medium", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addItemButton.ForeColor = System.Drawing.Color.White;
            this.addItemButton.Location = new System.Drawing.Point(19, 117);
            this.addItemButton.Name = "addItemButton";
            this.addItemButton.Size = new System.Drawing.Size(225, 40);
            this.addItemButton.TabIndex = 3;
            this.addItemButton.Text = "Add an Item";
            this.addItemButton.UseVisualStyleBackColor = false;
            this.addItemButton.Click += new System.EventHandler(this.AddItemButton_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Franklin Gothic Medium", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(81, 73);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(103, 15);
            this.label4.TabIndex = 2;
            this.label4.Text = "to the desired page.";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Franklin Gothic Medium", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(19, 58);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(219, 15);
            this.label3.TabIndex = 1;
            this.label3.Text = "Click the appropriate button to be redirected";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Franklin Gothic Medium", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.IndianRed;
            this.label2.Location = new System.Drawing.Point(17, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(229, 25);
            this.label2.TabIndex = 0;
            this.label2.Text = "Inventory Manager Portal";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.splitContainer1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button addItemButton;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button searchItemButton;
        private System.Windows.Forms.Button displayItemsButton;
        private System.Windows.Forms.Button restockItemButton;
        private System.Windows.Forms.Button removeItemButton;
    }
}

