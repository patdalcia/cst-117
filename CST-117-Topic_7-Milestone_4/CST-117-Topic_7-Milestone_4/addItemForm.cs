﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CST_117_Topic_7_Milestone_4
{
    public partial class addItemForm : Form
    {
        private item i;
        private bool flag;

        public addItemForm()
        {
            InitializeComponent();
        }

        private void AddItemForm_Load(object sender, EventArgs e)
        {
            
            
        }

        public item getData()
        {
            i = new item(addItemNameTextBox.Text, Int32.Parse(addItemQuantityTextBox.Text), addItemDepartmentTextBox.Text, Double.Parse(addItemPriceTextBox.Text));
            return i;
        }

        private void AddItemButton_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(addItemNameTextBox.Text))
            {
                if (!string.IsNullOrWhiteSpace(addItemDepartmentTextBox.Text))
                {
                    if (Int32.TryParse(addItemQuantityTextBox.Text, out int i) && i >= 0)
                    {
                        if (Double.TryParse(addItemPriceTextBox.Text, out double d) && d >= 0.0)
                        {
                            flag = true;
                            Close();
                            return;
                            
                        }
                        else
                        {
                            MessageBox.Show("Only positive numbers (decimals are okay) may be inputted for the item price field! Please try again.");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Only Whole positive numbers may be inputted for the Item Quantity field! Please try again.");
                    }
                }
            }
            MessageBox.Show("Please fill in all fields and then try again.");
        }

        private void AddItemCancelButton_Click(object sender, EventArgs e)
        {
            addItemNameTextBox.Clear();
            addItemDepartmentTextBox.Clear();
            addItemPriceTextBox.Clear();
            addItemQuantityTextBox.Clear();
            flag = false;
            Close();
        }

        public bool getFlag()
        {
            return this.flag;
        }

    }
}
