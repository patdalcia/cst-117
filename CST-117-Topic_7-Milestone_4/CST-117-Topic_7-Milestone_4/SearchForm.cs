﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CST_117_Topic_7_Milestone_4
{
    public partial class SearchForm : Form
    {
        private bool flag;

        public SearchForm()
        {
            InitializeComponent();
        }

        private void SearchButton_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrWhiteSpace(searchItemTextBox.Text))
            {
                if(!Int32.TryParse(searchItemTextBox.Text, out _))
                {
                    flag = true;
                    Close();
                }
            }
            else
            {
                MessageBox.Show("Please enter a valid search and try again.");
            }
        }

       

        private void SearchCancelButton_Click(object sender, EventArgs e)
        {
            searchItemTextBox.Clear();
            flag = false;
            Close();
        }

        public bool getFlag()
        {
            return this.flag;
        }

        public String getData()
        {
            return searchItemTextBox.Text;
        }

    }
}
