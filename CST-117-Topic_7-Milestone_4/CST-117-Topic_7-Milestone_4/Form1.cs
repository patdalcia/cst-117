﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CST_117_Topic_7_Milestone_4
{
    public partial class Form1 : Form
    {
        public static String itemName = "";
        public static String quantity = "";
        public static String department = "";
        public static String price = "";
        private InventoryManager manager = new InventoryManager();
        private List<item> inventory;

        public Form1()
        {
            InitializeComponent();
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            inventory = new List<item>();
        }

        private void AddItemButton_Click(object sender, EventArgs e)//When Add item button is clicked
        {
            Hide();
            inventory = manager.addItem(inventory); //adding item to inventory
            Show();
        }

        private void SearchItemButton_Click(object sender, EventArgs e) //Search for item in inventory
        {
            Hide();
            int i;
            i = manager.search(inventory); //getting item index
            if(i >= 0)
            {
                
                MessageBox.Show("ITEM FOUND ----> " + inventory[i].toString()); //showing item details
            }
            else if(i == -1)
            {}//do nothing
            else
            {
                MessageBox.Show("Search has no results! Please try again.");
            }
            Show();
        }

        private void DisplayItemsButton_Click(object sender, EventArgs e) //Displaying all items
        {
            String temp;
            temp = manager.displayAllItems(inventory); //Getting string containing all info
            if (!String.IsNullOrWhiteSpace(temp))
            {
                MessageBox.Show(temp); //Showing info
            }
            else //If inventory is empty
            {
                temp = "Inventory is empty, add some items to get started!";
                MessageBox.Show(temp);
            }
        }

        private void RemoveItemButton_Click(object sender, EventArgs e)
        {
            Hide();
            int temp = inventory.Count();
            inventory = manager.removeItem(inventory);
            if(temp == inventory.Count())
            {
                MessageBox.Show("Item removal has failed! Please try again, and double check search criteria.");
            }
            else if(temp > inventory.Count())
            {
                MessageBox.Show("Item has been sucsesfully removed!");
            }
            Show();
        }

        private void RestockItemButton_Click(object sender, EventArgs e)
        {
            var temp = inventory;
            inventory = manager.restockItem(inventory);
        }

        public static void displayMessage(String message)
        {
            MessageBox.Show(message);
        }
    }
}
