﻿namespace CST_117_Topic_7_Milestone_4
{
    partial class addItemForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.addItemPriceTextBox = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.addItemDepartmentTextBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.addItemQuantityTextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.addItemNameTextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.addItemCancelButton = new System.Windows.Forms.Button();
            this.addItemButton = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Location = new System.Drawing.Point(58, 42);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.AutoScroll = true;
            this.splitContainer1.Panel1.BackColor = System.Drawing.Color.IndianRed;
            this.splitContainer1.Panel1.Controls.Add(this.addItemPriceTextBox);
            this.splitContainer1.Panel1.Controls.Add(this.label8);
            this.splitContainer1.Panel1.Controls.Add(this.addItemDepartmentTextBox);
            this.splitContainer1.Panel1.Controls.Add(this.label7);
            this.splitContainer1.Panel1.Controls.Add(this.addItemQuantityTextBox);
            this.splitContainer1.Panel1.Controls.Add(this.label6);
            this.splitContainer1.Panel1.Controls.Add(this.addItemNameTextBox);
            this.splitContainer1.Panel1.Controls.Add(this.label5);
            this.splitContainer1.Panel1.Controls.Add(this.label1);
            this.splitContainer1.Panel1.ForeColor = System.Drawing.SystemColors.Control;
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.BackColor = System.Drawing.Color.White;
            this.splitContainer1.Panel2.Controls.Add(this.addItemCancelButton);
            this.splitContainer1.Panel2.Controls.Add(this.addItemButton);
            this.splitContainer1.Panel2.Controls.Add(this.label4);
            this.splitContainer1.Panel2.Controls.Add(this.label3);
            this.splitContainer1.Panel2.Controls.Add(this.label2);
            this.splitContainer1.Size = new System.Drawing.Size(684, 366);
            this.splitContainer1.SplitterDistance = 418;
            this.splitContainer1.TabIndex = 2;
            // 
            // addItemPriceTextBox
            // 
            this.addItemPriceTextBox.BackColor = System.Drawing.SystemColors.Control;
            this.addItemPriceTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.addItemPriceTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addItemPriceTextBox.Location = new System.Drawing.Point(184, 295);
            this.addItemPriceTextBox.Name = "addItemPriceTextBox";
            this.addItemPriceTextBox.Size = new System.Drawing.Size(220, 17);
            this.addItemPriceTextBox.TabIndex = 8;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(18, 294);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(115, 18);
            this.label8.TabIndex = 7;
            this.label8.Text = "Enter Item price:";
            // 
            // addItemDepartmentTextBox
            // 
            this.addItemDepartmentTextBox.BackColor = System.Drawing.SystemColors.Control;
            this.addItemDepartmentTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.addItemDepartmentTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addItemDepartmentTextBox.Location = new System.Drawing.Point(184, 236);
            this.addItemDepartmentTextBox.Name = "addItemDepartmentTextBox";
            this.addItemDepartmentTextBox.Size = new System.Drawing.Size(220, 17);
            this.addItemDepartmentTextBox.TabIndex = 6;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(18, 236);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(157, 18);
            this.label7.TabIndex = 5;
            this.label7.Text = "Enter Item department:";
            // 
            // addItemQuantityTextBox
            // 
            this.addItemQuantityTextBox.BackColor = System.Drawing.SystemColors.Control;
            this.addItemQuantityTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.addItemQuantityTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addItemQuantityTextBox.Location = new System.Drawing.Point(184, 177);
            this.addItemQuantityTextBox.Name = "addItemQuantityTextBox";
            this.addItemQuantityTextBox.Size = new System.Drawing.Size(220, 17);
            this.addItemQuantityTextBox.TabIndex = 4;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(18, 177);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(133, 18);
            this.label6.TabIndex = 3;
            this.label6.Text = "Enter Item quantity:";
            // 
            // addItemNameTextBox
            // 
            this.addItemNameTextBox.BackColor = System.Drawing.SystemColors.Control;
            this.addItemNameTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.addItemNameTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addItemNameTextBox.Location = new System.Drawing.Point(184, 117);
            this.addItemNameTextBox.Name = "addItemNameTextBox";
            this.addItemNameTextBox.Size = new System.Drawing.Size(220, 17);
            this.addItemNameTextBox.TabIndex = 2;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(18, 116);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(120, 18);
            this.label5.TabIndex = 1;
            this.label5.Text = "Enter Item name:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Franklin Gothic Medium", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(13, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(391, 47);
            this.label1.TabIndex = 0;
            this.label1.Text = "Add Item to Inventory";
            // 
            // addItemCancelButton
            // 
            this.addItemCancelButton.BackColor = System.Drawing.Color.IndianRed;
            this.addItemCancelButton.FlatAppearance.BorderColor = System.Drawing.Color.IndianRed;
            this.addItemCancelButton.FlatAppearance.BorderSize = 0;
            this.addItemCancelButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightCoral;
            this.addItemCancelButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightCoral;
            this.addItemCancelButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.addItemCancelButton.Font = new System.Drawing.Font("Franklin Gothic Medium", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addItemCancelButton.ForeColor = System.Drawing.Color.White;
            this.addItemCancelButton.Location = new System.Drawing.Point(16, 236);
            this.addItemCancelButton.Name = "addItemCancelButton";
            this.addItemCancelButton.Size = new System.Drawing.Size(225, 40);
            this.addItemCancelButton.TabIndex = 5;
            this.addItemCancelButton.Text = "Cancel";
            this.addItemCancelButton.UseVisualStyleBackColor = false;
            this.addItemCancelButton.Click += new System.EventHandler(this.AddItemCancelButton_Click);
            // 
            // addItemButton
            // 
            this.addItemButton.BackColor = System.Drawing.Color.IndianRed;
            this.addItemButton.FlatAppearance.BorderColor = System.Drawing.Color.IndianRed;
            this.addItemButton.FlatAppearance.BorderSize = 0;
            this.addItemButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightCoral;
            this.addItemButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightCoral;
            this.addItemButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.addItemButton.Font = new System.Drawing.Font("Franklin Gothic Medium", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addItemButton.ForeColor = System.Drawing.Color.White;
            this.addItemButton.Location = new System.Drawing.Point(16, 155);
            this.addItemButton.Name = "addItemButton";
            this.addItemButton.Size = new System.Drawing.Size(225, 40);
            this.addItemButton.TabIndex = 4;
            this.addItemButton.Text = "Confirm Changes";
            this.addItemButton.UseVisualStyleBackColor = false;
            this.addItemButton.Click += new System.EventHandler(this.AddItemButton_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Franklin Gothic Medium", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(81, 73);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(97, 15);
            this.label4.TabIndex = 2;
            this.label4.Text = "or cancel changes.";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Franklin Gothic Medium", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(19, 58);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(222, 15);
            this.label3.TabIndex = 1;
            this.label3.Text = "Click the appropriate button to either confirm";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Franklin Gothic Medium", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.IndianRed;
            this.label2.Location = new System.Drawing.Point(17, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(229, 25);
            this.label2.TabIndex = 0;
            this.label2.Text = "Inventory Manager Portal";
            // 
            // addItemForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.splitContainer1);
            this.Name = "addItemForm";
            this.Text = "addItemForm";
            this.Load += new System.EventHandler(this.AddItemForm_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button addItemCancelButton;
        private System.Windows.Forms.Button addItemButton;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox addItemNameTextBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox addItemQuantityTextBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox addItemDepartmentTextBox;
        private System.Windows.Forms.TextBox addItemPriceTextBox;
    }
}