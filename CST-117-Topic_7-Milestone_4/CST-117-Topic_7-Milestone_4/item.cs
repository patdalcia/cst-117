﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CST_117_Topic_7_Milestone_4
{
    public class item
    {
        private String itemName { get; set; }
        private int quantity { get; set; }
        private String department { get; set; }
        private double price { get; set; }
        

        public item(String name, int q, String d, double p) //Constructor
        {
            this.itemName = name;
            this.quantity = q;
            this.department = d;
            this.price = p;
        }
        public item(item i)
        {
            this.itemName = i.itemName;
            this.quantity = i.quantity;
            this.department = i.department;
            this.price = i.price;
        } //Alternate Constructor

        public String getName()
        {
            return this.itemName;
        }

        public void setName(String name)
        {
            this.itemName = name;
        }

        public String getDepartment()
        {
            return this.department;
        }

        public void setDepartment(String d)
        {
            this.department = d;
        }

        public int getQuantity()
        {
            return this.quantity;
        }

        public void setQuantity(int q)
        {
            this.quantity = q;
        }

        public void setPrice(double p)
        {
            this.price = p;
        }

        public double getPrice()
        {
            return this.price;
        }

        /*custom toString so I can customize the output*/
        public String toString()
        {
            return "Item Name: " + this.itemName + " Quantity: " + this.quantity + " Department: " + this.department + " Price: "
                + this.price + "\n";
        }
    }
}
