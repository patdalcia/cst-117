﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CST_117_Topic_7_Milestone_4
{
    class InventoryManager
    {

        List<item> inventory;
        SearchForm s = new SearchForm();

        public InventoryManager() { this.inventory = new List<item>(); }

        /*This method recieves a list and adds a new item to it. It then returns the updated list*/
        public List<item> addItem(List<item> list)
        {
            addItemForm f = new addItemForm();
            f.ShowDialog();
            if (f.getFlag())
            {
                list.Add(new item(f.getData()));
            }
            return list;
        }

        /*This method recieves a list, searches for desired critieria within list and return found index*/
        public int search(List<item> list)
        {
            s.ShowDialog();
            if (s.getFlag())
            {  
                String search = s.getData();
                for(int i = 0; i < list.Count(); i++)
                {
                    if(list[i].getName().ToLower() == search.ToLower() || list[i].getDepartment().ToLower() == search.ToLower())
                    {
                        return i;
                    }
                }
            }
            return -1; //If -1 returned, search has failed
        }

        /*This method recieves a list and returns all info contained within list in the form of a string*/
        public String displayAllItems(List<item> list)
        {
            String temp = "";
            foreach(item i in list)
            {
                temp += i.toString();
            }
            return temp;
        }

        /*This method recieves a list, it then calls the search method to find the index of item to be removed, it then removes 
         * this item*/
        public List<item> removeItem(List<item> list)
        {
            int index = search(list);
            if(s.getFlag())
            {
                list.RemoveAt(index);
            }
            return list;
        }

        /*This method recieves a list, it then calls the search method to find the index of item to be restocked, it then opens
         the restock item form to restock item*/
        public List<item> restockItem(List<item> list)
        {
            int index = search(list);
            if (index != -1)
            {
                RestockForm r = new RestockForm();
                r.ShowDialog();
                if (r.getFlag())
                {
                    if(Int32.TryParse(r.getData(), out _))
                    {
                        list[index].setQuantity(Int32.Parse(r.getData()));
                        Form1.displayMessage("Item has been restocked!");
                    }
                }
            }
            else
            {
                Form1.displayMessage("Desired item could not be found, please try again.");
            }
            return list;
        }
    }
}
