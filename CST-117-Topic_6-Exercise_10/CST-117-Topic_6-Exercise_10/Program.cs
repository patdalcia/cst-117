﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CST_117_Topic_6_Exercise_10
{
    class Program
    {
        static void Main(string[] args)
        {
            //Assigning file path
            string path = @"C:\Users\thegr\Desktop\CST-117\cst-117\CST-117-Topic_6-Exercise_10\CST-117-Topic_6-Exercise_10\Resources\testInputTextFile.txt";


            try
            {
                int count = 0;
                /*Reading text file*/
                String text = File.ReadAllText(path); 

                /*Converting to lowercase*/
                text = text.ToLower(); 

                /*Removing non letter characters*/
                text = new string((from c in text where char.IsWhiteSpace(c) || char.IsLetterOrDigit(c) select c).ToArray());
                
                /*seperating string into words*/
                String[] words = text.Split();
                     
                /*Using loop to count t and e*/
                    foreach(String s in words)
                    { 
                        if (s[s.Length - 1] == 't')
                        {
                            count++;
                        }
                        if (s[s.Length - 1] == 'e')
                        {
                            count++;
                        }
                    }
                        /*Displaying results to console*/
                        Console.WriteLine("There are " + count + " words that end in t or e");

            }
            catch (FileNotFoundException)
            {
                    Console.WriteLine("The file or directory cannot be found.");
            }

            }

        
    }
}
