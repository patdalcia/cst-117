﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CST_117_Topic_2_Programming_Exercise_2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void NameBox_SelectedIndexChanged(object sender, EventArgs e)
        {
           // outputLabel.Text = nameBox.SelectedItem.ToString();
        }

        private void SpeakButton_Click(object sender, EventArgs e)
        {
            String name = nameBox.SelectedItem.ToString();
            String color = "NO COLOR SELECTED";

            if (rB1.Checked)
            {
                color = rB1.Text.ToString();
            }
            if (rB2.Checked)
            {
                color = rB2.Text.ToString();
            }
            if (rB3.Checked)
            {
                color = rB3.Text.ToString();
            }
            if (rB4.Checked)
            {
                color = rB4.Text.ToString();
            }
            if (rB5.Checked)
            {
                color = rB5.Text.ToString();
            }
            if (rB6.Checked)
            {
                color = rB6.Text.ToString();
            }

            if (helloBox.Checked && byeBox.Checked)
            {
                outputLabel.Text = name + "'s favorite color is " + color + ", " + name + " says aloha!";
            }
            else if (helloBox.Checked)
            {
                outputLabel.Text = name + "'s favorite color is " + color + ", " + name + " says hello!";
            }
            else if (byeBox.Checked)
            {
                outputLabel.Text = name + "'s favorite color is " + color + ", " + name + " says goodbye!";
            }
            else
            {
                outputLabel.Text = name + "'s favorite color is " + color + ", " + name + " says nothing because nothing was selected!";
            }
        
        }
    }
}
