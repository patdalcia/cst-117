﻿namespace CST_117_Topic_2_Programming_Exercise_2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rB1 = new System.Windows.Forms.RadioButton();
            this.rB2 = new System.Windows.Forms.RadioButton();
            this.rB3 = new System.Windows.Forms.RadioButton();
            this.rB4 = new System.Windows.Forms.RadioButton();
            this.rB5 = new System.Windows.Forms.RadioButton();
            this.rB6 = new System.Windows.Forms.RadioButton();
            this.nameBox = new System.Windows.Forms.ListBox();
            this.helloBox = new System.Windows.Forms.CheckBox();
            this.byeBox = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.outputLabel = new System.Windows.Forms.Label();
            this.speakButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // rB1
            // 
            this.rB1.AutoSize = true;
            this.rB1.Location = new System.Drawing.Point(58, 180);
            this.rB1.Name = "rB1";
            this.rB1.Size = new System.Drawing.Size(45, 17);
            this.rB1.TabIndex = 0;
            this.rB1.TabStop = true;
            this.rB1.Text = "Red";
            this.rB1.UseVisualStyleBackColor = true;
            // 
            // rB2
            // 
            this.rB2.AutoSize = true;
            this.rB2.Location = new System.Drawing.Point(58, 204);
            this.rB2.Name = "rB2";
            this.rB2.Size = new System.Drawing.Size(54, 17);
            this.rB2.TabIndex = 1;
            this.rB2.TabStop = true;
            this.rB2.Text = "Green";
            this.rB2.UseVisualStyleBackColor = true;
            // 
            // rB3
            // 
            this.rB3.AutoSize = true;
            this.rB3.Location = new System.Drawing.Point(58, 227);
            this.rB3.Name = "rB3";
            this.rB3.Size = new System.Drawing.Size(46, 17);
            this.rB3.TabIndex = 2;
            this.rB3.TabStop = true;
            this.rB3.Text = "Blue";
            this.rB3.UseVisualStyleBackColor = true;
            // 
            // rB4
            // 
            this.rB4.AutoSize = true;
            this.rB4.Location = new System.Drawing.Point(58, 250);
            this.rB4.Name = "rB4";
            this.rB4.Size = new System.Drawing.Size(56, 17);
            this.rB4.TabIndex = 3;
            this.rB4.TabStop = true;
            this.rB4.Text = "Yellow";
            this.rB4.UseVisualStyleBackColor = true;
            // 
            // rB5
            // 
            this.rB5.AutoSize = true;
            this.rB5.Location = new System.Drawing.Point(58, 274);
            this.rB5.Name = "rB5";
            this.rB5.Size = new System.Drawing.Size(60, 17);
            this.rB5.TabIndex = 4;
            this.rB5.TabStop = true;
            this.rB5.Text = "Orange";
            this.rB5.UseVisualStyleBackColor = true;
            // 
            // rB6
            // 
            this.rB6.AutoSize = true;
            this.rB6.Location = new System.Drawing.Point(58, 298);
            this.rB6.Name = "rB6";
            this.rB6.Size = new System.Drawing.Size(55, 17);
            this.rB6.TabIndex = 5;
            this.rB6.TabStop = true;
            this.rB6.Text = "Purple";
            this.rB6.UseVisualStyleBackColor = true;
            // 
            // nameBox
            // 
            this.nameBox.FormattingEnabled = true;
            this.nameBox.Items.AddRange(new object[] {
            "Patrick",
            "Steve",
            "John ",
            "Susan",
            "Stephanie"});
            this.nameBox.Location = new System.Drawing.Point(58, 54);
            this.nameBox.Name = "nameBox";
            this.nameBox.Size = new System.Drawing.Size(120, 69);
            this.nameBox.TabIndex = 6;
            this.nameBox.SelectedIndexChanged += new System.EventHandler(this.NameBox_SelectedIndexChanged);
            // 
            // helloBox
            // 
            this.helloBox.AutoSize = true;
            this.helloBox.Location = new System.Drawing.Point(58, 377);
            this.helloBox.Name = "helloBox";
            this.helloBox.Size = new System.Drawing.Size(50, 17);
            this.helloBox.TabIndex = 7;
            this.helloBox.Text = "Hello";
            this.helloBox.UseVisualStyleBackColor = true;
            // 
            // byeBox
            // 
            this.byeBox.AutoSize = true;
            this.byeBox.Location = new System.Drawing.Point(58, 401);
            this.byeBox.Name = "byeBox";
            this.byeBox.Size = new System.Drawing.Size(69, 17);
            this.byeBox.TabIndex = 8;
            this.byeBox.Text = "Goodbye";
            this.byeBox.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label1.Location = new System.Drawing.Point(28, 348);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(110, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Say Hello? Goodbye?";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(31, 155);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(105, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Select Favorite Color";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(34, 24);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Select Name";
            // 
            // outputLabel
            // 
            this.outputLabel.AutoSize = true;
            this.outputLabel.Location = new System.Drawing.Point(289, 227);
            this.outputLabel.Name = "outputLabel";
            this.outputLabel.Size = new System.Drawing.Size(0, 13);
            this.outputLabel.TabIndex = 12;
            // 
            // speakButton
            // 
            this.speakButton.Location = new System.Drawing.Point(292, 346);
            this.speakButton.Name = "speakButton";
            this.speakButton.Size = new System.Drawing.Size(200, 72);
            this.speakButton.TabIndex = 13;
            this.speakButton.Text = "Speak";
            this.speakButton.UseVisualStyleBackColor = true;
            this.speakButton.Click += new System.EventHandler(this.SpeakButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(632, 450);
            this.Controls.Add(this.speakButton);
            this.Controls.Add(this.outputLabel);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.byeBox);
            this.Controls.Add(this.helloBox);
            this.Controls.Add(this.nameBox);
            this.Controls.Add(this.rB6);
            this.Controls.Add(this.rB5);
            this.Controls.Add(this.rB4);
            this.Controls.Add(this.rB3);
            this.Controls.Add(this.rB2);
            this.Controls.Add(this.rB1);
            this.Name = "Form1";
            this.Text = "Speaking Simulator";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton rB1;
        private System.Windows.Forms.RadioButton rB2;
        private System.Windows.Forms.RadioButton rB3;
        private System.Windows.Forms.RadioButton rB4;
        private System.Windows.Forms.RadioButton rB5;
        private System.Windows.Forms.RadioButton rB6;
        private System.Windows.Forms.ListBox nameBox;
        private System.Windows.Forms.CheckBox helloBox;
        private System.Windows.Forms.CheckBox byeBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label outputLabel;
        private System.Windows.Forms.Button speakButton;
    }
}

