﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CST_117_Topic_5_Exercise_9
{
    class Set
    {
        private List<int> elements;


        public Set()
        {
            elements = new List<int>();
        }

        /*added a copy method to allow me to keep track of original sets*/
        public void copy(Set s) 
        {
            for (int i = 0; i < s.elements.Count(); i++)
            {
                this.elements.Add(s.elements[i]); 
            }
        }

        public bool addElement(int val)
        {
            if (containsElement(val)) return false;
            else
            {
                elements.Add(val);
                return true;
            }
        }

        private bool containsElement(int val)
        {
            for (int i = 0; i < elements.Count; i++)
            {
                if (val == elements[i])
                    return true;
                //Else statement not allowing loop to iterate over full length of set
                /*
                else
                    return false; */
            }
            return false;
        }

        public override string ToString()
        {
            string str = "";
            foreach (int i in elements)
            {
                str += i + " ";
            }
            return str;
        }

        public void clearSet()
        {
            elements.Clear();
        }

        public Set union(Set rhs)
        {


            for (int i = 0; i < rhs.elements.Count; i++)
            {
                this.addElement(rhs.elements[i]);
            }

            //Returning rhs simply returns the second set instead of the new unioned set
            // return rhs;
            return this;
        }
    }
}
