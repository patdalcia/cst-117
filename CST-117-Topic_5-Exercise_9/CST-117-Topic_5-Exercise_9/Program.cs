﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CST_117_Topic_5_Exercise_9
{
    class Program
    {
        static void Main(string[] args)
        {
            //make some sets
            Set A = new Set();
            Set B = new Set();
            Set C = new Set(); //Adding new set to keep track of set A's original contents 

            //put some stuff in the sets
            Random r = new Random();
            for (int i = 0; i < 10; i++)
            {
                A.addElement(r.Next(4));
                B.addElement(r.Next(12));
            }
            //Copying to keep track of A's original contents
            C.copy(A);
            

            //display each set and the union
            Console.WriteLine("A: " + A);
            Console.WriteLine("B: " + B);
            Console.WriteLine("A union B: " + A.union(B).ToString());

            //revert A by copying c to it
            A.clearSet();
            A.copy(C);
            C.clearSet();
            
            //display original sets (should be unchanged)
            Console.WriteLine("After union operation");
            Console.WriteLine("A: " + A.ToString());
            Console.WriteLine("B: " + B);

            String s = "";
            while (s != "q")
            {
                Console.WriteLine("Enter 'q' to quit."); //This keeps the console open so results can be seen 
                s = Console.ReadLine();
            }


        }
    }
}