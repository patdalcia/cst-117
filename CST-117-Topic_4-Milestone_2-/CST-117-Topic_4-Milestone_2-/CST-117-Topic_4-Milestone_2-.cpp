// CST-117-Topic_4-Milestone_2-.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <string>



/*Start of the Item class*/
class item
{
public:

	std::string itemName;
	std::string department;
	int quantity = 0;

	item() { } //Default constructor

	item(std::string name, std::string d, int q) { //Non-default constructor
		itemName = name;
		department = d;
		quantity = q;
	}
	~item() { std::cout << "The destructor has been called" << std::endl; }//Destructor

	/*getters and setters*/
	std::string getName() {
		return itemName;
	}
	void setName(std::string name) {
		itemName = name;
	}
	std::string getDepartment() {
		return department;
	}
	void setDepartment(std::string d) {
		department = d;
	}
	int getQuantity() {
		return quantity;
	}
	void setQuantity(int q) {
		quantity = q;
	}
	/*To String Method*/
	std::string toString() {
		std::string q = std::to_string(quantity);
		return "Item Name: " + itemName + " Quantity: " + q + " Department: " + department;
	}
};

/*Driver method*/
int main()
{
	/*Creating test object of our item class*/
	item item_1("TEST NAME", "TEST DEPARTMENT", 1111);
	item item_2("test name", "test department", 2222);

	/*Displaying the toString method as well as the contents of the class objects*/
	std::cout << item_1.toString() << std::endl;
	std::cout << item_2.toString() << std::endl;
}