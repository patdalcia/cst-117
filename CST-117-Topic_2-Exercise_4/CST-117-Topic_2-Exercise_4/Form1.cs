﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CST_117_Topic_2_Exercise_4
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void Button1_Click(object sender, EventArgs e)
        {
            double x = 0.0;
            double minutes = 0.0;
            double hours = 0.0;
            double days = 0.0;

            if(Double.TryParse(secondsBox.Text, out x))
            {
                minutes = (double)x / 60.00;
                hours = (double)x / 3600.0;
                days = (double)x / 86400.0;

                if (x >= 86400.0)
                {
                    outputLabel.Text = "Seconds inputted >= 60........Minutes: " + minutes.ToString("0.###") + "   Seconds: " + x.ToString("0.###");
                    outputLabel2.Text = "Seconds inputted >= 3600........Hours: " + hours.ToString("0.###") + "   Seconds: " + x.ToString("0.###");
                    outputLabel3.Text = "Seconds inputted >= 86400........Days: " + days.ToString("0.###") + "   Seconds: " + x.ToString("0.###");
                }
               else if(x >= 3600.0)
                {
                    outputLabel.Text = "Seconds inputted >= 60........Minutes: " + minutes.ToString("0.###") + "   Seconds: " + x.ToString("0.###");
                    outputLabel2.Text = "Seconds inputted >= 3600........Hours: " + hours.ToString("0.###") + "   Seconds: " + x.ToString("0.###");
                }
               else if(x >= 60.0)
                {
                    outputLabel.Text = "Seconds inputted >= 60........Minutes: " + minutes.ToString("0.###") + "   Seconds: " + x.ToString("0.###");
                }
                else
                {
                    outputLabel.Text = "Negative numbers and zeros are not allowed, please try again.";
                    outputLabel2.Text = "";
                    outputLabel3.Text = "";
                }
            }
            else
            {
                outputLabel.Text = "Only numbers can be inputted. Please try again.";
            }
        }
    }
}
