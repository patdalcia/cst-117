﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace exercise_6
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void RollDiceButton_Click(object sender, EventArgs e)
        {
            String rolls = "Rolls:\n";
            Random random = new Random();
            int numSides = random.Next(4, 21);
            Dice leftDice = new Dice(6);
            Dice rightDice = new Dice(6);
            int x = 0;
            int y = 0;
            bool flag = false;

            while(flag == false)
            {
                leftDice.roll = rightDice.rollDie(); leftDice.numRolls++;
                rightDice.roll = rightDice.rollDie(); rightDice.numRolls++;

                resultsBox.Text += leftDice.roll.ToString() + "\t" + rightDice.roll.ToString() + System.Environment.NewLine;
                Console.WriteLine(leftDice.roll.ToString() + "\t" + rightDice.roll.ToString());

                if (leftDice.roll == 1 && rightDice.roll == 1)
                {
                    flag = true;
                    MessageBox.Show("SNAKE EYES found in " + leftDice.numRolls + " rolls.");
                    System.Windows.Forms.Application.Exit();
                }
               
            }
            
        }
    }
}
