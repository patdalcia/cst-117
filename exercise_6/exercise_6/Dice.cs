﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace exercise_6
{
    class Dice
    {
        public int numSides { get; set; }
        public int numRolls { get; set; }
        public int roll { get; set; }

        private Random random = new Random();


        public Dice(int sides)
        {
            this.numSides = sides;
        }

        public int rollDie()
        {
                int i = random.Next(1, numSides);
            
            
            return i;
        }
    }
}
