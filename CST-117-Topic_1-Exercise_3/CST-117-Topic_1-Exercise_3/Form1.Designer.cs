﻿namespace CST_117_Topic_1_Exercise_3
{
    partial class Exercise3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.convertNumberButton = new System.Windows.Forms.Button();
            this.feetInputBox = new System.Windows.Forms.TextBox();
            this.meterOutputLabel = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // convertNumberButton
            // 
            this.convertNumberButton.Location = new System.Drawing.Point(80, 111);
            this.convertNumberButton.Name = "convertNumberButton";
            this.convertNumberButton.Size = new System.Drawing.Size(110, 37);
            this.convertNumberButton.TabIndex = 0;
            this.convertNumberButton.Text = "Convert";
            this.convertNumberButton.UseVisualStyleBackColor = true;
            this.convertNumberButton.Click += new System.EventHandler(this.ConvertNumberButton_Click);
            // 
            // feetInputBox
            // 
            this.feetInputBox.Location = new System.Drawing.Point(23, 63);
            this.feetInputBox.Name = "feetInputBox";
            this.feetInputBox.Size = new System.Drawing.Size(76, 20);
            this.feetInputBox.TabIndex = 1;
            this.feetInputBox.TextChanged += new System.EventHandler(this.FeetInputBox_TextChanged);
            // 
            // meterOutputLabel
            // 
            this.meterOutputLabel.AutoSize = true;
            this.meterOutputLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.meterOutputLabel.Location = new System.Drawing.Point(161, 52);
            this.meterOutputLabel.Name = "meterOutputLabel";
            this.meterOutputLabel.Size = new System.Drawing.Size(0, 31);
            this.meterOutputLabel.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Franklin Gothic Medium", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(48, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(173, 21);
            this.label2.TabIndex = 3;
            this.label2.Text = "Feet to meters converter";
            this.label2.Click += new System.EventHandler(this.Label2_Click);
            // 
            // Exercise3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.ClientSize = new System.Drawing.Size(272, 168);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.meterOutputLabel);
            this.Controls.Add(this.feetInputBox);
            this.Controls.Add(this.convertNumberButton);
            this.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "Exercise3";
            this.Text = "Exercise 3";
            this.Load += new System.EventHandler(this.Exercise3_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button convertNumberButton;
        private System.Windows.Forms.TextBox feetInputBox;
        private System.Windows.Forms.Label meterOutputLabel;
        private System.Windows.Forms.Label label2;
    }
}

