﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CST_117_Topic_1_Exercise_3
{
    public partial class Exercise3 : Form
    {
        public Exercise3()
        {
            InitializeComponent();
        }

        private void Exercise3_Load(object sender, EventArgs e)
        {

        }

        private void Label2_Click(object sender, EventArgs e)
        {

        }

        private void ConvertNumberButton_Click(object sender, EventArgs e)
        {
            double num = 0.0;

            try
            {
                num = double.Parse(feetInputBox.Text);
                num = num / 3.281;
                meterOutputLabel.Text = num.ToString("0.###"); //Trimming output to exactly 3 decimal spaces
            }
            catch (System.DivideByZeroException exception) //Exception handling for divide by zero
            {
                MessageBox.Show("Exception Caught: " + exception);
            }
            catch (System.FormatException exception)
            {
                MessageBox.Show("Exception Caught: " + exception);
            }
        }

        private void FeetInputBox_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
